![IMG20210428102106_1](/uploads/27046f0da4457acb0d942ccc2888634f/IMG20210428102106_1.png)

![received_919014245311730_1](/uploads/31f8c513433019ceb01c51e3b7f23180/received_919014245311730_1.png)
![received_2703024603328473_1](/uploads/de84d4f43832b93ccb80e6940bef5754/received_2703024603328473_1.png)

# **1. JANUS**

- Clone janus server: https://github.com/meetecho/janus-gateway

- After clone success, janus will general a socket server --> get it for mobile(wss://janus.conf.meetecho.com/ws : this is janus dev server)

# **2. MOBILE**

1. Add Janus to source

- Get janus file from src. Then import to your source

- Init Janus:

```
let started = false
let sfutest

Janus.init({
  debug: 'all',
  callback: function() {
    if (started) {
      return
    }
    started = true
  }
})

```

- Start Janus:

```
const startJanus = () => {
    console.log('startJanus')
    janus = new Janus({
      server: Your Server(socket server above),
      success: () => {
        janus.attach({
          plugin: 'janus.plugin.videocall',
          success: (pluginHandle) => {
            sfutest = pluginHandle
            // When janus is attached successfully, register a username(same as init a connection in socket)
            let register = {
              request: 'register',
              username: myusername.toString()
            }
            sfutest.send({ message: register })
          },
          error: (error) => {
            console.log('  -- Error attaching plugin...', error)
          },
          consentDialog: (on) => {
            console.log('consentDialog on')
          },
          mediaState: (medium, on) => {
            console.log('mediaState on', medium)
          },
          webrtcState: (on) => {
            console.log('webrtcState on')
          },
          onmessage: (msg, jsep) => {
            // Method listen event like has a call, call was accepted...
          },
          onlocalstream: (stream) => {
            console.log(
              '----------------STREAM onlocalstream------------- ',
              stream
            )
          },
          onremotestream: (stream) => {
            console.log(
              '----------------STREAM onremotestream------------- ',
              stream
            )
          },
          oncleanup: () => {

          }
        })
      },
      error: (err) => {
        console.log('err', err)
      },

      destroyed: () => {
        console.log('Janus was destroyed')
      }
    })
  }

```

2. Implement some action

- Get list user online:

```
{
        "request" : "list"
}

```

- Call:

```
{
        "request" : "call",
        "username" : "<username to call>"
}

```

==> See more on : [https://janus.conf.meetecho.com/docs/videocall.html](https://janus.conf.meetecho.com/docs/videocall.html)

3. Wake up app from background/quit

- Add https://rnfirebase.io/messaging/usage (Read carefully)

- Android:

  - NodeModule --> @react-native-firebase --> messaging --> android/src/main/java/io/invertase/firebase/messaging --> ReactNativeFirebaseMessagingReceiver.java

  ```
    //  |-> ---------------------
    //    App in Background/Quit
    //   ------------------------

    try {
      Intent backgroundIntent = new Intent(context, ReactNativeFirebaseMessagingHeadlessService.class);
      backgroundIntent.putExtra("message", remoteMessage);
      ComponentName name = context.startService(backgroundIntent);
      if (name != null) {
        HeadlessJsTaskService.acquireWakeLockNow(context);
        String packageName = context.getPackageName(); // <-- Add this line
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName); // <-- Add this line
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // <-- Add this line
        context.startActivity(launchIntent); // <-- Add this line
      }
    }

  ```

  NOTE:

  - On android 10 must request ACTION_MANAGE_OVERLAY_PERMISSION to wake up app. Follow this: [https://www.npmjs.com/package/rn-android-overlay-permission](https://www.npmjs.com/package/rn-android-overlay-permission)

  - When app was locked, put this code on MainActivity.java

  ```
      if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
        setShowWhenLocked(true);
        setTurnScreenOn(true);
      }
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

  ```

  ![Screen_Shot_2021-04-29_at_4.52.05_PM](/uploads/354296617206b829c0577f67855c3776/Screen_Shot_2021-04-29_at_4.52.05_PM.png)

- IOS:

  - Add [react-native-callkeep](https://github.com/react-native-webrtc/react-native-callkeep) and [react-native-voip-push-notification](https://github.com/react-native-webrtc/react-native-voip-push-notification)

  - Backend: Add [node-apn](https://github.com/node-apn/node-apn) to push noti wake up app()

  NOTE: The topic is bundleId.voip
