![alt](https://miro.medium.com/max/2732/1*EU1vvTKmcWukfBOj2MrjfQ.png)

# **1. CONCEPT**

> CI: Continuous Delivery is a step beyond Continuous Integration. Your application is not only built and tested at every code change pushed to the codebase, but, as an additional step, it’s also deployed continuously, though the deployments are triggered manually. This method ensures the code is checked automatically but requires human intervention to manually and strategically trigger the deployment of the changes.

> CD: Continuous Deployment is also a further step beyond Continuous Integration, similar to Continuous Delivery. The difference is that instead of deploying your application manually, you set it to be deployed automatically. It does not require human intervention at all to have your application deployed.

# **2. REQUIREMENTS**

### **_Before you continue, ensure you meet the following requirements:_**

```
- AppCenter account
- File build: keystore(release), provisioning, .p12
- Source Gitlab, Github ...
```

# **3. SETUP**

## 1. Connect source Gitlab

> Go to Appcenter --> Choose your app --> Build --> Gitlab(or another one) --> Choose source that you want from Gitlab(Make sure your are owner of the source)

![Screen_Shot_2020-12-30_at_09.54.49](/uploads/ccdd57e86d3a3ee6e99aa7cea7442ad9/Screen_Shot_2020-12-30_at_09.54.49.png)

![Screen_Shot_2020-12-30_at_09.55.22](/uploads/6e9b774582dd3f9c225117804be5f81f/Screen_Shot_2020-12-30_at_09.55.22.png)

## 2. Config build

#### - Choose the branch that you want to config

![Screen_Shot_2020-12-30_at_10.01.43](/uploads/c0d59fa03ff4eed692084a94e779ad2d/Screen_Shot_2020-12-30_at_10.01.43.png)
<br/>
<br/>

#### - Press on Config build button or Setting icon on the right of the branch

![Screen_Shot_2020-12-30_at_10.02.47](/uploads/d5be7852be289f57bcea5526d713aa62/Screen_Shot_2020-12-30_at_10.02.47.png)
![Screen_Shot_2020-12-30_at_10.03.12](/uploads/8749795736bc795e82dcf9fb34141f5d/Screen_Shot_2020-12-30_at_10.03.12.png)

#### - Some information

![Screen_Shot_2020-12-30_at_10.07.29](/uploads/b3ce02177d27a51cc28afe3f7a4f48d1/Screen_Shot_2020-12-30_at_10.07.29.png)
![Screen_Shot_2020-12-30_at_10.08.04](/uploads/2f65c0acd7b31e8d5cd86c93efc0aa38/Screen_Shot_2020-12-30_at_10.08.04.png)

##### \* Build frequency: There are 2 options

```
  + Build this branch on every push: Whenever you push new code on the branch that you chose, the build will be implement
  + Manually choose when to run builds: You must manually build by click Build now button
```

##### \* Environment variables

```
Variables in .env file of your source
```

##### \* Sign builds

**ANDROID**

```

- If your app was setup to build(Recommend) : Tick on My Gradle settings are entirely set to handle signing automatically

- If not: Provide keystore file and enter keystore password, key alias, key alias password


```

![Screen_Shot_2020-12-30_at_10.19.59](/uploads/11559cc90d8e0098d0d584b6bd04e855/Screen_Shot_2020-12-30_at_10.19.59.png)

![Screen_Shot_2020-12-30_at_10.21.03](/uploads/45dcc6c5fa52acb06c1f8f4b0b9c8740/Screen_Shot_2020-12-30_at_10.21.03.png)

**IOS**

```

You must provide provisioning prodfile and .p12 file to install on the real device. If your source use OneSignal, you must also provide provisioning prodfile of OneSignal


```

![Screen_Shot_2020-12-30_at_10.26.30](/uploads/5e8f1a4cf1c4d477612fd4c66e56c59b/Screen_Shot_2020-12-30_at_10.26.30.png)

[--> How to create provisioning prodfile](https://developer.apple.com/forums/thread/47806)

[--> How to export .p12 file](https://help.attendify.com/en/articles/441408-how-to-export-a-distribution-certificate-as-a-p12-file)

##### \* Test on real device(Not recommend)

If you want to test on real device, please create device first

```
Go to Test -->  Device sets --> New device set -->  Choose the devices that you want

```

![Screen_Shot_2020-12-30_at_10.39.00](/uploads/dddacad858fc30687defa97c55b56420/Screen_Shot_2020-12-30_at_10.39.00.png)

##### \* Distribute builds

**Android**

- Choose the distribute Groups(Recommend) and then choose the group that you want. If you want to distribute to Store, your app must be published first.

  ![Screen_Shot_2020-12-30_at_10.53.54](/uploads/3ea860fec93f7625d4ec687583cc426b/Screen_Shot_2020-12-30_at_10.53.54.png)

  - You can create groups to distribute app by click on Group --> Add group

  ![Screen_Shot_2020-12-30_at_10.50.45](/uploads/9e3c4a6499683b4043461b3f9781c7b2/Screen_Shot_2020-12-30_at_10.50.45.png)

  - Install Appcenter on your phone to test app

**IOS**

- Groups: Like Android

- Store: You must connect the store first

  > Stores --> Connect to Store --> App Store Connect --> Add your account

  **_NOTE: If Appcenter requires the password special. Please go to [https://appleid.apple.com/account/manage](https://appleid.apple.com/account/manage) --> Generate password --> Enter whatever you want --> Create --> Copy the password was generated_**

# **OK, it was done. Now, let's save and build !**

You can config like below:

![Screen_Shot_2020-12-30_at_11.12.33](/uploads/6dc8b2b5254c4ac5c12a0a917724d1e1/Screen_Shot_2020-12-30_at_11.12.33.png)

![Screen_Shot_2020-12-30_at_11.12.50](/uploads/db5b1bc9ad5fa79a836b123b31ec0065/Screen_Shot_2020-12-30_at_11.12.50.png)

![Screen_Shot_2020-12-30_at_11.13.02](/uploads/ae1ebca4d6546c03cf681b7518891d38/Screen_Shot_2020-12-30_at_11.13.02.png)

![Screen_Shot_2020-12-30_at_11.13.18](/uploads/59afd3a813e124aebb566cd70c5d8ae0/Screen_Shot_2020-12-30_at_11.13.18.png)

For Android:

![Screen_Shot_2020-12-30_at_11.15.22](/uploads/c9b4d20e175d92e68e511b6140f27a31/Screen_Shot_2020-12-30_at_11.15.22.png)
<br/>

# **Tips: [Reduce build time](https://github.com/microsoft/appcenter/issues/473#issuecomment-734939770)**
